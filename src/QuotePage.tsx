import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { useState } from 'react';
import { useLocation } from "react-router-dom";
import quotes from "./qoutes.json";
import QuoteCard from './quoteCard';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';

export default function QuotePage(): any{
    const location = useLocation();
    let character: string = location.state.character;
    let image: string = location.state.image;
    var quotesList: string[] = quotes[character];
    const [currentQuote, setCurrentQuote] = useState(0);
    return(
        <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
        >
            <QuoteCard 
                name={character}
                image={image}
                quote={quotesList[currentQuote]}
            />
            <Button
                variant="outlined"
                sx = {{
                    marginTop: "40px"
                }}
                onClick = {() => {
                    if(currentQuote < quotesList.length - 1){
                        setCurrentQuote(currentQuote + 1);
                    }
                    else{
                        setCurrentQuote(0);
                    }
                }}
            >get aother quote</Button>
        </Grid>
    );
}