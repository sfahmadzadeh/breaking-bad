import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';
import CharacterCard from './characterCard';
import images from "./images.json";
import backgroundImg from "./assets/background4.jpg"
import { Box } from "@mui/system";
import { Grid, TextField, MenuItem, CircularProgress, Typography } from '@mui/material';

export default function CharatersList(): any{
    const [characters, setCharacters] = useState([]);
    const [charactersData, setCharactersData] = useState([]);
    const [currentFilteredCharacerts, setCurrentFilteredCharacerts] = useState([]);
    const [currentSortedCharacerts, setCurrentSortedCharacerts] = useState([]);
    const [backToDefault, setBackToDefault] = useState(true);
    //let backToDefault: boolean = true;
    let catchedError: boolean = false;

    interface CharacterInterface {
        name: string;
        nickname: string;
        birthday: string;
        image: string;
        statusInMovie: string;
    }

    const getData = async () => {
        const res = await axios.get("https://mocki.io/v1/32bc3bf4-a6dc-4507-94d9-7b36b0b0014b")
        .catch(() => {catchedError = true;});
        setCharactersData(res.data["characters"]);
        setCurrentFilteredCharacerts(res.data["characters"]);
        setCurrentSortedCharacerts(res.data["characters"]);
        let list = createCards(res.data["characters"]);
        setCharacters(list);
    };

    useEffect(() => {
        getData();
    }, []);

    if(catchedError){
        return(
            <Typography>Some thing went wrong. Please try again later.</Typography>
        );
    }

    else if(charactersData.length > 0){
        return(
            <Grid
                container
                direction="row"
                justifyContent="center"
            >
                <TextField 
                    fullWidth
                    id="outlined"
                    label="filter"
                    placeholder="enter name or nickname"
                    onChange={ event => {
                        if(event.target.value === ""){
                            if(backToDefault){
                                setCharacters(createCards(charactersData));
                                setCurrentFilteredCharacerts(charactersData);
                                setBackToDefault(false);
                            }
                            else{
                                setCharacters(createCards(currentSortedCharacerts));
                                setCurrentFilteredCharacerts(charactersData);
                                setBackToDefault(true);
                            }
                        }
                        else{
                            let filteredCharacters: CharacterInterface[] = [];
                            for (let character of currentSortedCharacerts){
                                if(character["name"].includes(event.target.value) || character["nickname"].includes(event.target.value)){
                                    filteredCharacters.push(character);
                                }
                            }
                            setBackToDefault(false);
                            setCharacters(createCards(filteredCharacters));
                            setCurrentFilteredCharacerts(filteredCharacters);
                        }
                    }}
                />
                <TextField 
                    fullWidth
                    id="outlined"
                    select
                    label="select sorting mode"
                    sx= {{marginTop: "20px"}}
                    onChange = {event => {
                        if(event.target.value === "name"){
                            let currentCopy: CharacterInterface[] = [...currentFilteredCharacerts];
                            currentCopy.sort((a, b) => a.name.localeCompare(b.name));
                            setCurrentSortedCharacerts(currentCopy);
                            setCharacters(createCards(currentCopy));
                            setBackToDefault(false);
                        }
                        else if(event.target.value === "nickname"){
                            let currentCopy: CharacterInterface[] = [...currentFilteredCharacerts];
                            currentCopy.sort((a, b) => a.nickname.localeCompare(b.nickname));
                            setCurrentSortedCharacerts(currentCopy);
                            setCharacters(createCards(currentCopy));
                            setBackToDefault(false);
                        }
                        else if(event.target.value === "birthday"){
                            let currentCopy: CharacterInterface[] = [...currentFilteredCharacerts];
                            currentCopy.sort((a, b) => new Date(b.birthday) - new Date(a.birthday));
                            setCurrentSortedCharacerts(currentCopy);
                            setCharacters(createCards(currentCopy));  
                            setBackToDefault(false); 
                        }
                        else{
                            if(backToDefault){
                                setCurrentSortedCharacerts(charactersData);
                                setCharacters(createCards(charactersData));
                                setBackToDefault(false);
                            }
                            else{
                                setCurrentSortedCharacerts(charactersData);
                                setCharacters(createCards(currentFilteredCharacerts));
                                setBackToDefault(true);
                            }
                        }
                    }}
                >
                    {["name", "nickname", "birthday", "default"].map(mode =>
                        (<MenuItem key={mode} value={mode}>
                        {mode}
                        </MenuItem>)
                    )}
                </TextField>
                {characters}
            </Grid>
        );
    }
    else{
        return(
            <CircularProgress />
        );
    }
}

function createCards(characters: any): any{
    const cards = characters.map((character: any) => {
        return(
            <CharacterCard 
                name={character["name"]}
                nickname={character["nickname"]}
                birthday={character["birthday"]}
                statusInMovie={character["status"]}
                //image={character["image"]}
                image={images["urls"][character["name"]]}
            />
        );
    });
    return cards;
}