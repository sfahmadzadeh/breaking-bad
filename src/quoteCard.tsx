import { Card, CardContent, CardMedia, Typography } from '@mui/material';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

export default function QuoteCard(props: any): any{
    return(
        <Card
            sx = {{
                display: "flex",
                alignItems: "center",
                width: "70%",
                height: "400px"
            }}
        >
            <CardMedia 
                component="img"
                image={props.image}
                alt={props.name}
                sx={{maxWidth: "180px", maxHeight: "280px", marginLeft: "15px"}}
            />
            <CardContent sx={{marginLeft: "15px"}}>
                <Typography>{props.quote}</Typography>
                <br />
                <br />
                <Typography sx={{fontSize: "30px"}}>{"~" + props.name}</Typography>
            </CardContent>
        </Card>
    );
}