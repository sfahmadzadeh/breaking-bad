import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { useNavigate } from "react-router-dom";
import { Card, CardActionArea, CardContent, CardMedia, Typography } from '@mui/material';

export default function CharacterCard(props: any): any{
    const navigate = useNavigate();
    const quotesPage = () => {
        navigate("/quote", {
            state: {
                character: props.name,
                image: props.image
            }
        })
    }
    return(
        <Card
            sx = {{
                width: "250px",
                margin: "60px"
            }}
        >
            <CardActionArea
                 ButtonBase
                 onClick = {quotesPage}
            >
                <CardMedia
                    component="img"
                    height="240px"
                    image={props.image}
                    alt={props.name}
                />
                <CardContent>
                    <Typography sx={{fontSize: "30px"}}>
                        {props.name}
                    </Typography>
                    <br />
                    <Typography>
                        Nickname: {props.nickname}
                        <br />
                        birthday: {props.birthday}
                        <br />
                        status in movie: {props.statusInMovie}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}