import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Routes, Route, BrowserRouter } from "react-router-dom";
import CharatersList from './src/CharactersList';
import QuotePage from './src/QuotePage';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<CharatersList />}></Route>
                <Route path="/quote" element={<QuotePage />}></Route>
            </Routes>
        </BrowserRouter>
    );
}
export default App;